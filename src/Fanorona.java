
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import Views.ControllerView;

public class Fanorona extends Application{
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/plateau.fxml"));
		Scene scene=new Scene(root);
		stage.setScene(scene);
		stage.setTitle("FANORONA");
		stage.setResizable(false);
		stage.show();
		 Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
	     stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
	     stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
	}

}
