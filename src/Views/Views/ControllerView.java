package Views;
//import java.beans.EventHandler;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import com.sun.istack.internal.logging.Logger;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Duration;


public class ControllerView implements Initializable{
	

    @FXML
    private Button newGame;

    @FXML
    private Button newGamePC;

    @FXML
    private Button load;

    @FXML
    private Button settings;

    @FXML
    private Button rules;

    @FXML
    private Button quit;
    
    
    @FXML
    private Button quitFinal;

    @FXML
    private Button backQuit;
    
    @FXML
    private ProgressIndicator progress;
    
    private boolean bool=false;
    
    @FXML
    private Button fermerok;
    
    @FXML
    private AnchorPane panlancAnchor;
    
    @FXML
    private Button commence2;
    @FXML
	void Commence2(ActionEvent event) throws IOException{
             	
		//	try {
				Pane root;
				 root = (Pane)FXMLLoader.load(getClass().getResource("/Views/fano.fxml"));
				 
				 Scene scene= commence2.getScene();
	    		 root.translateYProperty().set(scene.getHeight()); 
	    		// StackPane parentContainer = (StackPane) scene.getRoot();
	    		 panlancAnchor.getChildren().add(root);
	    		 Timeline timeline = new Timeline();
	    		 KeyValue key = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
	    		 KeyFrame kf = new KeyFrame(Duration.seconds(1), key);
	    		 timeline.getKeyFrames().add(kf);
	    		/*timeline.setOnFinished(event1 ->{
	    		parentContainer.getChildren().remove(panlancAnchor);
	    		 });*/
	    		 timeline.play();
			/*} catch (IOException e) {
		    Logger.getLogger(Views.ControllerView.class.getName(), null).log(Level.SEVERE, null, e);
			}*/ 
    		
        }
    
  

   class bg_Thread implements Runnable{
	   
	@Override
	public void run() {
		for(int i=0;i<=100;i++)
		{
			//progress.setProgress(i / 100.0);
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (NullPointerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}
	   
   }
   
    
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		Thread th = new Thread(new bg_Thread());
		th.start();
		

		//progress.setProgress(0.0);
		//commencer.setDisable(true);
		
		
	}
	
	
	public void centrer(Stage stage) {
		Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
	    stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
	    stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
	}
	
	
	
	/*fenetre principale */
	
	
	
	@FXML
    void Quit(ActionEvent event) throws Exception {
		 
		 Stage stage = new Stage();
		 Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/Pane_jeu_quitter.fxml")); 
		 Scene scene=new Scene(root);
		 stage.setScene(scene);		
		 stage.show();
		 
		 
	}
	
	
	 @FXML
	    void Rules(ActionEvent event)  throws Exception {   	         	
			/*try {*/
				Parent root;
				root = FXMLLoader.load(getClass().getResource("/Views/Pane_jeu_reglesdujeu.fxml"));
				 Scene scene= settings.getScene();
	    		 root.translateXProperty().set(scene.getWidth());
	    		// StackPane parentContainer = (StackPane) scene.getRoot();
	    		 optionAnchor.getChildren().add(root);
	    		 Timeline timeline = new Timeline();
	    		 KeyValue key = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
	    		 KeyFrame kf = new KeyFrame(Duration.seconds(2), key);
	    		 timeline.getKeyFrames().add(kf);
	    		/*timeline.setOnFinished(event1 ->{
	    		parentContainer.getChildren().remove(panlancAnchor);
	    		 });*/
	    		 timeline.play();

	    }
	 
	 @FXML
	    private AnchorPane optionAnchor;
	  @FXML
	    void Settings(ActionEvent event) throws Exception {
		  
			 System.out.println("GGFGGF ZERBO MOHAMED");   	         	
				/*try {*/
					Parent root;
					root = FXMLLoader.load(getClass().getResource("/Views/options.fxml"));
					 Scene scene= settings.getScene();
		    		 root.translateXProperty().set(scene.getWidth());
		    		// StackPane parentContainer = (StackPane) scene.getRoot();
		    		 optionAnchor.getChildren().add(root);
		    		 Timeline timeline = new Timeline();
		    		 KeyValue key = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
		    		 KeyFrame kf = new KeyFrame(Duration.seconds(3), key);
		    		 timeline.getKeyFrames().add(kf);
		    		/*timeline.setOnFinished(event1 ->{
		    		parentContainer.getChildren().remove(panlancAnchor);
		    		 });*/
		    		 timeline.play();
				/*} catch (IOException e) {
			    Logger.getLogger(Views.ControllerView.class.getName(), null).log(Level.SEVERE, null, e);
				} 
	    		*/

	    }

	  @FXML

	  void NewGamePC(ActionEvent event) throws Exception { 	
		  Stage stage =  (Stage) newGamePC.getScene().getWindow();
		  
		  stage.setX(10);
		  stage.setY(5);

		  Parent root;
			 root =(Parent) FXMLLoader.load(getClass().getResource("/Views/plateau.fxml"));
				 Scene scene= newGamePC.getScene();
	    		 root.translateXProperty().set(scene.getWidth());
	    		// StackPane parentContainer = (StackPane) scene.getRoot();
	    		 optionAnchor.getChildren().add(root);
	    		 Timeline timeline = new Timeline();
	    		 KeyValue key = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
	    		 KeyFrame kf = new KeyFrame(Duration.seconds(3), key);
	    		 timeline.getKeyFrames().add(kf);
	    		 timeline.play();
	    		 stage.setHeight(701);
	   		  stage.setWidth(1325);
	    }
	  
	  @FXML	  
	  public void fermerok(ActionEvent event) {
		  Stage stage = (Stage) fermerok.getScene().getWindow();
		  stage.close();
	  }
	 
	  @FXML
	   public void NewGame(ActionEvent event) throws Exception {
		  Stage stage =  (Stage) newGame.getScene().getWindow();
		  stage.setHeight(720);
		  stage.setWidth(1345);
		 // stage.setX(value);
		
			//FXMLLoader loader = new FXMLLoader(getClass().getResource("/Views/fano.fxml"));
		  Parent root;
	      root =(Parent) FXMLLoader.load(getClass().getResource("/Views/plateau.fxml"));
		 Scene scene= newGame.getScene();		
  		 root.translateXProperty().set(scene.getWidth());
  		// StackPane parentContainer = (StackPane) scene.getRoot();
  		optionAnchor.getChildren().add(root);
  		 Timeline timeline = new Timeline();
  		 KeyValue key = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
  		 KeyFrame kf = new KeyFrame(Duration.seconds(2), key);
  		 timeline.getKeyFrames().add(kf);
  		 timeline.play();
	    	

	    }
	  
	  
	  
	  
	  
	  
	  /*--fenetre options--*/
	  @FXML
	    private Button backOptions;
	 @FXML
	    private AnchorPane paramAnchor;
	    @FXML
	    void BackOptions(ActionEvent event) throws Exception {
	    
	    	Parent root;
			root = FXMLLoader.load(getClass().getResource("/Views/fano.fxml"));
			 Scene scene= backOptions.getScene();
    		 root.translateXProperty().set(scene.getWidth());
    		// StackPane parentContainer = (StackPane) scene.getRoot();
    		 paramAnchor.getChildren().add(root);
    		 Timeline timeline = new Timeline();
    		 KeyValue key = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
    		 KeyFrame kf = new KeyFrame(Duration.seconds(2), key);
    		 timeline.getKeyFrames().add(kf);
    		/*timeline.setOnFinished(event1 ->{
    		parentContainer.getChildren().remove(panlancAnchor);
    		 });*/
    		 timeline.play();
	    }
	    
	    /*-- fenetre Difficulte Joueur vs IA*/
	    @FXML
	    private Button backDifficuleIA;

	    @FXML
	    void BackDifficulteIA(ActionEvent event) throws Exception {
		     Stage stage = (Stage) backDifficuleIA.getScene().getWindow();
			 Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/fanorona.fxml")); 
			 Scene scene=new Scene(root);
			 stage.setScene(scene);		
			 stage.show();

	    }
	  
	  
	  /*Fenetre Rules*/
	  
	  
	  
	  @FXML
	    private Button backRules;
	  @FXML
	  private AnchorPane reglejeuAnchor;
	    @FXML
	    void BackRules(ActionEvent event) throws Exception {
	    	Parent root;
			root = FXMLLoader.load(getClass().getResource("/Views/fano.fxml"));
			 Scene scene= backRules.getScene();
    		 root.translateXProperty().set(scene.getWidth());
    		// StackPane parentContainer = (StackPane) scene.getRoot();
    		 reglejeuAnchor.getChildren().add(root);
    		 Timeline timeline = new Timeline();
    		 KeyValue key = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
    		 KeyFrame kf = new KeyFrame(Duration.seconds(2), key);
    		 timeline.getKeyFrames().add(kf);
    		/*timeline.setOnFinished(event1 ->{
    		parentContainer.getChildren().remove(panlancAnchor);
    		 });*/
    		 timeline.play();
	    	

	    }
	    
	    
	    
	    
	    /* Quitter le jeu */
	    
	    @FXML
	    void BackQuit(ActionEvent event) throws Exception {
	    	Stage stage = (Stage) backQuit.getScene().getWindow();
	    	stage.close();

	    }

	    @FXML
	    void QuitFinal(ActionEvent event) {
	 	    	System.exit(0);
	    }
	  
	  
	
	    /* Game */
	    
	    @FXML
	    private Button rulesInGame;

	    @FXML
	    void RulesInGame(ActionEvent event) throws IOException {
	    	
	    	 Stage stage = new Stage(); 
	    	 
			 Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/Pane_jeu_reglesdujeu _Game.fxml")); 
			 Scene scene=new Scene(root);
			 stage.setScene(scene);		
			 stage.show();

	    }
	    
	    

	    @FXML
	    private Button backRulesGame;

	    @FXML
	    void BackRulesGame(ActionEvent event) {
	    	Stage stage = (Stage) backRulesGame.getScene().getWindow();
	    	stage.close();

	    	

	    }
	    
	    
	 
	    //private Button backMenu;

	   /* @FXML
	    void BackMenu(ActionEvent event) throws IOException {
	    	
	    	Stage stage = (Stage) backMenu.getScene().getWindow(); 
			 Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/fanorona.fxml")); 
			 Scene scene=new Scene(root);
			 stage.setScene(scene);		
			 stage.show();
			 this.centrer(stage);

	    }*/
	    @FXML
	    private Button backMenu;

         public Stage gameWindow;
        // private Stage gamewind;
         //private Stage stage1;
	    
	    @FXML
	    void BackMenu(ActionEvent event) throws IOException {
			// gameWindow =(Stage) backMenu.getScene().getWindow();
			 Stage stage = new Stage();
			 Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/Pane_jeu_quitter_partie.fxml")); 
			 Scene scene=new Scene(root);
			 stage.setScene(scene);		
			 stage.show();
			 //final Node source = (Node) event.getSource();
			 /// stage1 = (Stage) source.getScene().getWindow();

	    }
	  
	    @FXML
	    private Button validerQuitterPart;

	    @FXML
	    private Button backQuitterPart;

	    @FXML
	    void BackQuitterPart(ActionEvent event) {
	    	Stage stage = (Stage) backQuitterPart.getScene().getWindow();
	    	stage.close();
	    	

	    }
	    
	    
	    @FXML
	    void ValiderQuitterPart(ActionEvent event) throws IOException {
	    	 //stage1.close();
	    	 Stage stage = (Stage) backQuitterPart.getScene().getWindow(); 
	          Pane root = (Pane)FXMLLoader.load(getClass().getResource("/Views/fanorona.fxml"));
	 
	    	 Scene scene=new Scene(root);
			 stage.setScene(scene);	
			// stage1.close();
			// gamewind.hide();
			/* if(gameWindow != null) {
				 gameWindow.hide();
				 ((Stage) gameWindow).close();
				 gameWindow = null;
			 }*/
			// stage.close();	   	  
	    }
	    
	    @FXML
	    private Button pjdIA1;
	    void pjdIA1(ActionEvent event) throws IOException {
		   // pjdIA1.setOnMouseEntered(arg0);

	    }
	   
	    @FXML
	    private Button pjdIA2;

	    @FXML
	    private Button pjdIA3;


	  
	  
    
    

}
