package Modele;


import java.io.FileInputStream;
import java.io.*;
import java.util.Scanner;
class Test{

  public Plateau plateauFichier(String nomF){
    Plateau P = new Plateau();
    try{
      InputStream flux=new FileInputStream(nomF);
      InputStreamReader lecture=new InputStreamReader(flux);
      BufferedReader buff=new BufferedReader(lecture);
      char c;
      int i=0,j=0;
      do{
        c=(char)buff.read();
        if(c=='O' || c=='X' || c=='.' || c=='\n'){
          if(c=='\n') {i=0; j++; System.out.println();}
          else{
            if(c=='.') c=' ';
            P.setCase(i,j,c);
            i++;
          }
        }
      }while(j<5);

    }catch (Exception e){
      System.out.println(e.toString());
      }
    P.afficher();
    return P;
  }

}
