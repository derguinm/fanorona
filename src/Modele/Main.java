package Modele;

import java.util.ArrayList;


import Controleur.Joueur;
import Controleur.JoueurHumain;

public class Main {

	public static void main(String... argc) {
		Plateau P = new Plateau();
		History<Coup> H=new History<Coup>();
		Joueur JH1=new JoueurHumain(1,P);
		P.init();
		P.afficher();
		int mangeur;
		
		mangeur=P.prise(3, 2, 4, 2, 1).size();	
		Coup c=new Coup(3, 2, 4, 2,mangeur,JH1);
		H.faire(c);
		P.afficher();
		mangeur=P.prise(5, 3, 5, 2, 1).size();
		Coup c2=new Coup(5, 3, 5, 2, mangeur,JH1);
		H.faire(c2);
		P.afficher();
		mangeur=P.prise(3, 3, 3, 2, 1).size();
		System.out.println("mangeur = "+mangeur);
		Coup c3=new Coup(3,3,3,2,mangeur,JH1);
		H.faire(c3);
		P.afficher();
		H.annuler();
		H.annuler();
		H.refaire();
		
		P.afficher();
		mangeur=P.prise(4, 3, 5, 2, 1).size();	
		Coup c4=new Coup(4, 3, 5, 2,mangeur,JH1);
		H.faire(c4);
		P.afficher();
		mangeur=P.prise(4, 2, 4, 3, 2).size();
		System.out.println("heeeey "+mangeur);
		Coup c5=new Coup(4, 2, 4, 3,-mangeur,JH1);
		H.faire(c5);
		P.afficher();
		H.annuler();
		P.afficher();
	}

}
