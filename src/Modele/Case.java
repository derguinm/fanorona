package Modele;


public class Case{
  private int x;
  private int y;
  private char contain;

public Case(int x,int y,char contain){
this.x=x;
this.y=y;
this.contain=contain;
}
public Case()
{
	
}
public boolean IsEmpty(){
  return this.contain==' ';
}
public char GetContain(){
  return this.contain;
}
public void SetContain(char contain){
  this.contain=contain;
}
public int GetX(){
  return this.x;
}

public int GetY(){
  return this.y;
}

public void SetY(int y){
  this.y=y;
}


public void SetX(int x){
  this.x=x;
}

public void print_case(){
	System.out.print("( "+this.x+" ; "+this.y+" )");
	System.out.println();
}

public boolean isFree() {
		if(this.GetContain()==' ')
			return true;
		else
      return false;
}

public boolean accessible(Case cible)
{
	// cas pour le pion peut se déplacer a la diagonale*
	//vérifie la position du pion choisis
	if(this.x%2 == this.y%2)
		{
			if(Math.abs(this.x%2 - cible.GetX()) == Math.abs(this.y%2 - cible.GetY()) == true )
				return true;
			else
				return false;
		}
	// cas ou il doit se deplacer qu'horizentale ou la verticale
	else
		{
		if(Math.abs(this.x - cible.GetX()%2) + Math.abs(this.y - cible.GetY()%2)== 1)
								return true;
			else
								return false;

		}
}
}
