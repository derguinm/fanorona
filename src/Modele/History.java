    package Modele;
import java.util.*;


import Structures.*;

public class History<E extends Commande>{
  SequenceListe<Coup> passee, futur;

    History(){
      passee=new SequenceListe<Coup>();
      futur=new SequenceListe<Coup>();
    }

    public boolean peutAnnuler(){
      return !passee.estVide();
    }

    public boolean peutRefaire(){
      return  !futur.estVide();
    }

    public  Coup transfert(SequenceListe<Coup> src, SequenceListe<Coup> dst){
      if(!src.estVide())
      {
        Coup cc=src.extraitTete();
        dst.insereTete(cc);
        return cc;
      }
      else {
        System.out.println("erreur : pile src vide");
        return null;
      }

    }
    

    public void annuler(){
      Coup a=transfert(passee,futur);
      a.desexecute();
    }
    //  depiler passée + desexec + empiler future

    public void refaire(){
      //depiler future + exec
      Coup a=transfert(futur,passee);
      a.desexecute();

    }

    public void faire(Coup nouveau){
      nouveau.execute();
      passee.insereTete(nouveau);
    }
}
