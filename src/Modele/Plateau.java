package Modele;

import java.util.*;


public class Plateau {
	private Case[][] plateau=new Case[9][5];
	private Case pion_selected;
	 	
public Plateau(){
	int i=0,j=0;
	for (j = 0; j < 5; j++) {

		for (i = 0; i < 9; i++) {
			Case c = new Case(i,j,' ');
			plateau[i][j]=c;
		}

	}
	init();
	pion_selected=new Case();
	pion_selected.SetX(-1);
	pion_selected.SetY(-1);

}
	// Grille debut du jeu
	public void init() {
		for(int j=0;j<2;j++)
			for(int i=0;i<9;i++) {

				plateau[i][j].SetContain('O');

			}


		for(int i=0;i<9;i++) {
			if(i<5) {
				if(i%2==0) this.plateau[i][2].SetContain('O');
				else this.plateau[i][2].SetContain('X');
			}
			else {
				if(i%2==0)
				this.plateau[i][2].SetContain('X');
				else
				this.plateau[i][2].SetContain('O');
			}
		}

		this.plateau[4][2].SetContain(' ');

		for(int j=3;j<5;j++) {
			for(int i=0;i<9;i++) {
				this.plateau[i][j].SetContain('X');
			}
		}

	}

	public void afficher(){
        System.out.print("   ");
        for(int k=0; k<9; k++)
            System.out.print(k+"  ");
        System.out.println("\n");
        for(int j=0; j<5; j++){
            System.out.print(j+"  ");
            for(int i=0; i<9; i++){
                System.out.print(this.plateau[i][j].GetContain()+"  ");
            }
            System.out.println();
        }
        System.out.println();
    }

	public Case getCase(int x, int y){
					return plateau[x][y];
		}

	/*public void select() {
		int x,y;
		Scanner sc = new Scanner(System.in);
		do {
			x=sc.nextInt();
			y=sc.nextInt();
		}while(plateau[x][y].isFree());
		System.out.println("Vous etes sur le pion ( "+x+" ; "+y+" )");
	}
	 */
	public boolean isIn(int x, int y){
			if(x<0 || y<0 || x>8 || y>4)
				return false;
			else
				return true;
	}

	public boolean pionJoueur(int x, int y, int player){
		if(player==1) return (getCase(x,y).GetContain()=='X');
		if(player==0) return (getCase(x,y).GetContain()=='O');
		else
			return false;

	}

	//Retourne vrai si la case est vide, faux sinon
	public boolean IsEmpty(int i,int j){
			if(!isIn(i,j)) return false;
			return (this.plateau[i][j].GetContain()==' ');
	}

	public ArrayList<Case> accessibles(int x, int y){
			ArrayList<Case> L = new ArrayList<Case>();
			if(x%2 == y%2){
				if(isIn(x-1,y-1) && IsEmpty(x-1,y-1) ) L.add(plateau[x-1][y-1]);
				if(isIn(x,y-1) && IsEmpty(x,y-1)) L.add(plateau[x][y-1]);
				if(isIn(x+1,y-1) && IsEmpty(x+1,y-1)) L.add(plateau[x+1][y-1]);
				if(isIn(x-1,y) && IsEmpty(x-1,y)) L.add(plateau[x-1][y]);
				if(isIn(x+1,y) && IsEmpty(x+1,y)) L.add(plateau[x+1][y]);
				if(isIn(x-1,y+1) && IsEmpty(x-1,y+1)) L.add(plateau[x-1][y+1]);
				if(isIn(x,y+1) && IsEmpty(x,y+1)) L.add(plateau[x][y+1]);
				if(isIn(x+1,y+1) && IsEmpty(x+1,y+1)) L.add(plateau[x+1][y+1]);
		}
		else{
				if(isIn(x,y-1) && IsEmpty(x,y-1)) L.add(plateau[x][y-1]);
				if(isIn(x-1,y) && IsEmpty(x-1,y)) L.add(plateau[x-1][y]);
				if(isIn(x+1,y) && IsEmpty(x+1,y)) L.add(plateau[x+1][y]);
				if(isIn(x,y+1) && IsEmpty(x,y+1)) L.add(plateau[x][y+1]);
		}
		return L;
	}

	public void setCase(int x, int y, char c){
		plateau[x][y]= new Case(x,y,c);
	}

	public void supprimerPion(int x, int y){
		setCase(x,y,' ');
	}

	public ArrayList<Case> adjacentes(int x, int y){
				ArrayList<Case> L = new ArrayList<Case>();
				if(x%2 == y%2){
				if(isIn(x-1,y-1)) L.add(plateau[x-1][y-1]);
				if(isIn(x,y-1)) L.add(plateau[x][y-1]);
				if(isIn(x+1,y-1)) L.add(plateau[x+1][y-1]);
				if(isIn(x-1,y)) L.add(plateau[x-1][y]);
				if(isIn(x+1,y)) L.add(plateau[x+1][y]);
				if(isIn(x-1,y+1)) L.add(plateau[x-1][y+1]);
				if(isIn(x,y+1)) L.add(plateau[x][y+1]);
				if(isIn(x+1,y+1)) L.add(plateau[x+1][y+1]);
		}
		else{
				if(isIn(x,y-1)) L.add(plateau[x][y-1]);
				if(isIn(x-1,y)) L.add(plateau[x-1][y]);
				if(isIn(x+1,y)) L.add(plateau[x+1][y]);
				if(isIn(x,y+1)) L.add(plateau[x][y+1]);
		}
		return L;
	}

	public void printCaseList(ArrayList<Case> L){
		Iterator<Case> it=  L.iterator();
		while(it.hasNext()){
			it.next().print_case();
			System.out.println();
		}
	}

	// Retourne une liste avec les cases a un déplacement implique une prise de pion(s)
	public ArrayList<Case> peutManger(int x, int y){
				char mangeur=getCase(x,y).GetContain();
				int x0,x1;
				int y0,y1;
				ArrayList<Case> La = accessibles(x,y);
				ArrayList<Case> L = new ArrayList<Case>();
				Iterator<Case> it=  La.iterator();
				while(it.hasNext()){
					Case cible = it.next();
					x1=cible.GetX();
					y1=cible.GetY();
					x0=(x1-x)*2;
					y0=(y1-y)*2;
					if(isIn(x+x0,y+y0) && verify(x+x0,y+y0,mangeur))
						L.add(cible);

				}
				//printCaseList(L);
				return L;
	}
	public int endOfGame() {
		if(pionsJoueur(1).size()==0) return 2;
		if(pionsJoueur(2).size()==0) return 1;
		else return 0;
	}
	
	public ArrayList<Case> adversairesAdjacents(int x, int y){
		ArrayList<Case> La = adjacentes(x,y);
		char allie=getCase(x,y).GetContain();
		ArrayList<Case> L = new ArrayList<Case>();
		Iterator<Case> it=  La.iterator();
		while(it.hasNext()){
			Case cible = it.next();
			if(cible.GetContain()!=allie && cible.GetContain()!=' ')
				L.add(cible);
		}
		return L;
	}
	
	

	public ArrayList<Case> peutAspirer(int x, int y){
				int x0;
				int y0;
				ArrayList<Case> La = adversairesAdjacents(x,y);
				ArrayList<Case> L = new ArrayList<Case>();
				Iterator<Case> it=  La.iterator();
				while(it.hasNext()){
					Case cible = it.next();
					x0=(2*x-cible.GetX());
					y0=(2*y-cible.GetY());
					if(IsEmpty(x0,y0))
						L.add(getCase(x0,y0));
				}
				return L;

	}

	public ArrayList<Case> peutPrendre(int x, int y){
		ArrayList<Case> L1 = peutManger(x,y);
		//System.out.println("L1:");
		//printCaseList(L1);
		ArrayList<Case> L2 = peutAspirer(x,y);
		//System.out.println("L2:");
		//printCaseList(L2);
		ArrayList<Case> L = new ArrayList<Case>();
		L.addAll(L1);
		L.addAll(L2);
		return L;
	}

	// retourne vrai si on peut manger
	public boolean verify(int x, int y, char mangeur){
				char cible=' ';
				if(mangeur=='X') cible = 'O';
				if(mangeur=='O') cible = 'X';
				return(cible==getCase(x,y).GetContain());

	}

	// Retourne vrai si le pion peut se déplacer de src vers dest
	public boolean coup_possible(int x1, int y1, int x2, int y2){
		if(getCase(x1,y1).GetContain()==' ') { System.out.println("Coup impossible"); return false; }
		ArrayList<Case> L = accessibles(x1,y1);
		Iterator<Case> it=  L.iterator();
			while(it.hasNext()){
				if(it.next()==getCase(x2,y2))
					return true;
			}
		return false;
	}

	public boolean coup_valide(int x1, int y1, int x2, int y2, int player){
		ArrayList<Case> L = peutPrendre(x1,y1);
		//ArrayList<Case> L2 = peutAspirer(x1,y1,player);
		if(!estMangeur(x1,y1,player)){
			if(coup_possible(x1,y1,x2,y2) && choixPionCorrect(x1,y1,player)) return true;
			else{
				System.out.println("DEPLACEMENT IMPOSSIBLE");
				return false;
			}
		}
		else{
			if(contientPion(L,x2,y2)!=0 && coup_possible(x1,y1,x2,y2) && choixPionCorrect(x1,y1,player))
				return true;
			else{
				System.out.println("DEPLACEMENT IMPOSSIBLE 2");
				return false;
			}
		}

	}

	// Retourne l'ensemble des pions pouvant manger pour le joueur numéro player
	public ArrayList<Case> pionsMangeurs(int player){
		char p=' ';
		ArrayList<Case> pionsM = new ArrayList<Case>();
		ArrayList<Case> tempL1 = new ArrayList<Case>();
		ArrayList<Case> tempL2 = new ArrayList<Case>();
		if(player==1) p='X';
		if(player==0) p='O';
		for(int j=0; j<5; j++){
				for(int i=0; i<9; i++){
					if(getCase(i,j).GetContain()==p){
						tempL1=peutManger(i,j);
						tempL2=peutAspirer(i,j);
						if(tempL1.size()!=0)
							pionsM.add(getCase(i,j));
						if(tempL2.size()!=0)
								pionsM.add(getCase(i,j));
					}
					tempL1.clear();
					tempL2.clear();
				}
		}
		return pionsM;
	}

	// renvoie la liste des pions à prendre selon le mode (aspiration ou collision) pour un deplacement donné
	public ArrayList<Case> prise(int x1, int y1, int x2, int y2, int mode){
		ArrayList<Case> L = new ArrayList<Case>();
		
		
		char allie=getCase(x1,y1).GetContain();
		char ennemi;
		int i,x0,y0;
		
		if(allie=='O') 
			ennemi='X';   
		else 
			ennemi='O';
		
		if(mode==1){
			i=2;
			while(isIn(x1+(x2-x1)*i,y1+(y2-y1)*i) && getCase(x1+(x2-x1)*i,y1+(y2-y1)*i).GetContain()==ennemi){
				x0=x1+(x2-x1)*i;
				y0=y1+(y2-y1)*i;
				L.add(getCase(x0,y0));
				i++;
			}
			return L;
		}
		else{
			i=1;
			while(isIn(x1+(x1-x2)*i,y1+(y1-y2)*i) && getCase(x1+(x1-x2)*i,y1+(y1-y2)*i).GetContain()==ennemi){
				x0=x1+(x1-x2)*i;
				y0=y1+(y1-y2)*i;
				L.add(getCase(x0,y0));
				i++;
			}
			return L;
		}
	}


	public boolean choixPionCorrect(int x, int y, int player){
		if(!pionJoueur(x,y,player)) 
			return false;
		
		ArrayList<Case> L = pionsMangeurs(player);
		
		if(L.size()!=0)
			return L.contains(getCase(x,y));
		else return true;
	}

	public void effacerPions(ArrayList<Case> L){
		Iterator<Case> it=  L.iterator();
		while(it.hasNext()){
			Case c = it.next();
			setCase(c.GetX(),c.GetY(),' ');
		}
	}

	/*public void deplacement(int x1, int y1, int x2, int y2, int player, int mode){
		if(coup_valide(x1,y1,x2,y2,player)){
			ArrayList<Case> L = prise(x1,y1,x2,y2,mode);
			setCase(x2,y2,getCase(x1,y1).GetContain());
			setCase(x1,y1,' ');
			effacerPions(L);
		}
	}*/

	public int contientPion(ArrayList<Case> L, int x, int y){
		Iterator<Case> it=  L.iterator();
		int occ=0;
		while(it.hasNext()){
			Case c=it.next();
			if(c.GetX()==x && c.GetY()==y) occ++;
		}
		return occ;
	}

	public boolean estMangeur(int x, int y, int player){
		ArrayList<Case> L = pionsMangeurs(player);
		
		if(contientPion(L,x,y)!=0) return true;
		else return false;
	}


	public ArrayList<Case> deplacement(int x1, int y1, int x2, int y2, int mode){
			ArrayList<Case> L = prise(x1,y1,x2,y2,mode);
			setCase(x2,y2,getCase(x1,y1).GetContain());
			setCase(x1,y1,' ');
			effacerPions(L);
			return L;
	}
	

	public Case choix_aleatoire(int player){

		char c=' ';

		int rand_num=-1;

		if(player==1) c='X';

		else c='O';

		Random rand = new Random();

		ArrayList<Case> L=new ArrayList<Case>();

		L=pionsMangeurs(player);

		if(L.size()==0)

		//L=pionsJoueurs(player);



		rand_num= rand.nextInt(L.size()+1);



		return L.get(rand_num);

	}


	public ArrayList<Case> pionsJoueur(int player){
		ArrayList<Case> L = new ArrayList<Case>();
		char allie;
		if (player == 1) {
			allie = 'X';
		}else
			allie = 'O';
		
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 9; i++) {
				if (getCase(i, j).GetContain() == allie) {
					L.add(getCase(i, j));
				}
			}
		}
		return L;
	}
	


 	public Case choix_dep(){

	ArrayList<Case> L = new ArrayList<Case>();

	Random rand = new Random();

		if(estMangeur(0, 0, 0))
		{
			// listes des deplacements pour manger.
		}else

		L=accessibles(0, 0);

		int rand_num= rand.nextInt(L.size()+1);

		return (Case) L.get(rand_num);

		// l� sera d�cid� le mouvement a faire entre la case appelant (src) et le resultat envoy�(dest)

	}
 	static ArrayList<Case> chemin =new ArrayList<Case>();;
 	static int player_now=1;
 	public boolean combo_test(int x1, int y1, int x2, int y2,int player){
		 
		if (chemin.size() == 0) {
			chemin.add(getCase(x1, y1));
		}
		if (player_now == player) {
			int directionx=x2-x1;
			int directiony=y2-y1;
			
			ArrayList<Case> M=peutPrendre(x2, y2);
			M=subList(M, chemin);
			if (M != null ) {
				if (M.size()>1) {
					chemin.add(getCase(x2, y2));
				}else {
					if (! (M.size() == 1) && (M.get(0).GetX()-x2)!= directionx && (M.get(0).GetY()-y2)!= directiony) {
						return false;
					}
				}
				
				return true;
			}else {
					return false;
			}
		}else {
			player_now = (player_now+1) % 2;
			chemin.clear();
			return combo_test(x1, y1, x2, y2, player_now);
		}
		
		
 	}
 	public boolean verif_combo(int x1,int y1,int x2,int y2,ArrayList<Case> chemin,int directionx,int directiony)
	{
		int direction2x=x1-x2;
		int direction2y=y1-y2;
		ArrayList<Case> L=peutPrendre(x1,y1);
		L=subList(L,chemin);
		if((L!= null) && search(L,x2,y2) && (directionx != direction2x || directiony != direction2y))
			return true;
		else
			return false;
	}
	public boolean search(ArrayList<Case> L,int x,int y){
		Iterator<Case> it=L.iterator();
		while(it.hasNext())
		{
			Case a=it.next();
			if((a.GetX()==x) && (a.GetY()==y))
			return true;
		}
			return false;

	}
	//validééé
	public ArrayList<Case> subList(ArrayList<Case> a,ArrayList<Case> b){
		Iterator<Case> it_a=a.iterator();
		Iterator<Case> it_b=b.iterator();
		Case c1,c2;
		if(a==null || a.size()==0)
			return null;
		while(it_b.hasNext())
		{
				c1 = it_b.next();
			while(it_a.hasNext()){
			 	c2 = it_a.next();
				if(c1.GetX()==c2.GetX() && c1.GetY()==c2.GetY())
				it_a.remove();
			}
		}
		if(a.size()==0)return null;
									 return a;
	}
 
}
