package Controleur;
/*
 * Morpion pédagogique
 * Copyright (C) 2016 Guillaume Huard

 * Ce programme est libre, vous pouvez le redistribuer et/ou le
 * modifier selon les termes de la Licence Publique Générale GNU publiée par la
 * Free Software Foundation (version 2 ou bien toute autre version ultérieure
 * choisie par vous).

 * Ce programme est distribué car potentiellement utile, mais SANS
 * AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de
 * commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
 * Licence Publique Générale GNU pour plus de détails.

 * Vous devez avoir reçu une copie de la Licence Publique Générale
 * GNU en même temps que ce programme ; si ce n'est pas le cas, écrivez à la Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
 * États-Unis.

 * Contact: Guillaume.Huard@imag.fr
 *          Laboratoire LIG
 *          700 avenue centrale
 *          Domaine universitaire
 *          38401 Saint Martin d'Hères
 */

import java.util.ArrayList;
import java.util.Scanner;

import Modele.Case;
import Modele.Plateau;
import javafx.scene.paint.Color;

public class JoueurHumain extends Joueur {
	static int n=0;
	public static int x1,y1;
	public JoueurHumain(int n, Plateau p) {
		super(n, p);
	}

	
	public static int getY1() {
		return y1;
	}
	public static int getX1() {
		return x1;
	}
	
	
	public boolean jeu1(int i, int j, int jc) {
			if (plateau.choixPionCorrect(i,j,jc)){
			 	// selectionner le pion IHM
				 //System.out.println("pion selectionne"+i+","+j);
			 	x1=i;
			 	y1=j;
			 	n++;
			 	return true;
			 	}
				
		return false;
}
	public static int mode=-1;
	@Override
	ArrayList<Case> jeu(int i, int j, int jc) {
		ArrayList<Case> Lprise=new ArrayList<Case>();
				if ((x1==i)&&(y1==j)) {
					//deselectionner le pion sur lIHM
					n=0;
					
				}else
				{			
					if(plateau.coup_valide(x1, y1, i, j, jc)) {
						
						if((plateau.prise(x1, y1, i, j, 1).size() > 0)  && (plateau.prise(x1, y1, i, j, 2).size() > 0) ){
							//aspi et percu
							//demande au user
							if (mode != -1) {
								Lprise=plateau.prise(x1, y1, i, j, mode);
								plateau.deplacement(x1, y1, i, j, mode);	
							}
							mode=-1;
							Lprise.add(plateau.getCase(x1, y1));
							return Lprise;
													
						}
						else
						{
							if (plateau.prise(x1, y1, i, j, 1).size() > 0) {
								//percution
								
								Lprise=plateau.prise(x1, y1, i, j, 1);
								plateau.deplacement(x1, y1, i, j, 1);
								
							}else
							{
								//aspi
								Lprise=plateau.prise(x1, y1, i, j, 2);
								plateau.deplacement(x1, y1, i, j, 2);

							}							
						}
						
					}else {
						return null;
					}
					n=0;	
				}
			return Lprise;
	}
	
	
	
}
