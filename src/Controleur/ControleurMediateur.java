package Controleur;


import java.util.ArrayList;

import Modele.*;
import Views.ControllerView;

public class ControleurMediateur {
	Plateau plateau;
	ControllerView cv;
	
	Joueur [] joueurs;
	public int joueurCourant=1;
	//final int lenteurAttente = 50;
	//int decompte;

	// JOueur 1 X
	// Joueur 2 O

	public ControleurMediateur(Plateau p, ControllerView c, boolean[] IA) {
		
		plateau = p;
		cv = c;
		joueurs = new Joueur[IA.length];
		for (int i = 0; i < joueurs.length; i++)
			if (IA[i])
				joueurs[i] = new JoueurIA(i, plateau);
			else
				joueurs[i] = new JoueurHumain(i, plateau);
	}


	public boolean premier_clic(int x,int y) {
		return (joueurs[joueurCourant].jeu1(x, y, joueurCourant));
		
	}
	
	
	public ArrayList<Case> clicSouris(int x, int y) {	
			return joueurs[joueurCourant].jeu(x, y, joueurCourant);	
	}

	public void changeJoueur() {
		joueurCourant = (joueurCourant + 1) % joueurs.length;
		//decompte = lenteurAttente;
	}


	
	/*public void tictac() {
		if (jeu.enCours()) {
			if (decompte == 0) {
				if (joueurs[joueurCourant].tempsEcoule()) {
					changeJoueur();
				} else {
					System.out.println("On vous attend, joueur " + joueurs[joueurCourant].num());
					decompte = lenteurAttente;
				}
			} else {
				decompte--;
			}
		}
	}*/
}
